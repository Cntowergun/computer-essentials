﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using EngineML.Model;
using Microsoft.ML;
using OntarioCOVIDAPIl;

namespace Ontario_COVID_19_Case_Estimator
{
    class Program
    {
        static async Task Main(string[] args)
        {
            try
            {
                Console.WriteLine("Hello World!");
                var Input = new ModelInput();
                var Covid = OntarioCovid.FromJson(new WebClient().DownloadString("http://covid.bigheados.com/Ontario.txt"));
                Input.PendingTests = float.Parse(Covid.OntarioPendingTests.Replace(",", ""));
                Input.PositiveRate = (float)Covid.OntarioPositiveRate;
                Input.TestsYesterday = float.Parse(Covid.OntarioYesterdayTest);
                var Output = ConsumeModel.Predict(Input);
                Console.WriteLine(Output.Score.ToString("0"));
                File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Estimate.txt", Output.Score.ToString("0"));
            }
            catch (Exception ex)
            {
                if (Directory.Exists(@"C:\Users\Administrator\Desktop\COVID\ML"))
                {
                    File.WriteAllText("C:\\Users\\Administrator\\Desktop\\COVID\\ML\\error.txt", ex.ToString()); 
                }
                Console.WriteLine(ex);
            }
        }
    }
}
