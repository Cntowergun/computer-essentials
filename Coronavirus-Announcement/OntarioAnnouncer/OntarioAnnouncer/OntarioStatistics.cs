﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using OntarioVaccineStatisticsAPI;
using ScreenShotDemo;

namespace OntarioAnnouncer
{
    public partial class OntarioStatistics : Form
    {
        public OntarioStatistics()
        {
            InitializeComponent();
        }

        OntarioVaccineStatistics VaccineData;

        private void AddToVaccine(string d)
        {
            if(OntarioVaccineStatistics.Text == "")
            {
                OntarioVaccineStatistics.Text = d;
            }
            else
            {
                OntarioVaccineStatistics.Text += "\n" + d;
            }
        }
        private string COVIDDirectory = @"C:\Users\Administrator\Desktop\COVID";
        string COVIDDirectoryDebug = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\COVID";

        private async void OntarioStatistics_Load(object sender, EventArgs e)
        {
            VaccineData = OntarioVaccineStatisticsAPI.OntarioVaccineStatistics.FromJson(new WebClient().DownloadString("http://covid.bigheados.com/ontariovaccine/statistics.txt"));
            AddToVaccine("Fully vaccinated: " + VaccineData.FullyVaccinated);
            AddToVaccine("People who have got the first dose: " + VaccineData.FirstDose);
            AddToVaccine("Percentage of people who got the first dose: " + VaccineData.OntarioFirstDosePercentage);
            AddToVaccine("Last Updated: " + VaccineData.LastUpdated);
            AddToVaccine("Doses delivered: " + VaccineData.DosesDelivered);
            List<string> RegionsList = new List<string>();
            foreach (var region in VaccineData.Regions)
            {
                RegionsList.Add(region.Name + ", Fully Vaccinated: " + region.FullyVaccinated + ", Total Doses Administered: " + region.TotalDosesAdministered);
            }
            string[] RegionsData = RegionsList.OfType<string>().ToArray();
            foreach (string s in RegionsData)
            {
                RegionsStatistics.Text += RegionsStatistics.Text == "" ? s : "\n" + s;
            }
            await Task.Delay(1000);
            string ImageFile = Directory.Exists(COVIDDirectory) ? COVIDDirectory + "\\image" + "\\" + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Year.ToString() + "vaccine.png" : COVIDDirectoryDebug + "\\image" + "\\" + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Year.ToString() + "vaccine.png";
            ScreenCapture Capture = new ScreenCapture();
            Capture.CaptureWindowToFile(this.Handle,ImageFile,ImageFormat.Png);
            Close();
        }
    }
}
