﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IronOcr;
using ScreenShotDemo;

namespace Auto_Matrix_Submitter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            IronOcr.Installation.LicenseKey = "IRONOCR.CNTOWERGUN.22915-42C1B20742-DNVIAH-FXQP64L4JFIA-FYH3LFMMRI2P-TVRFOLOMKQKY-B4BHEGACCVD5-22WCDBAYDWSB-UFMB2H-TRL2WLD7UXWDEA-DEPLOYMENT.TRIAL-ZRUHZL.TRIAL.EXPIRES.28.DEC.2021";
            Text = "Auto Matrix Inputter - Better than autohotkey";
        }

        bool Stop = false;

        private async void Form1_Load(object sender, EventArgs e)
        {
            TopMost = true;
            ocr = new IronTesseract();
            button2.Visible = false;
        }

        private async Task FillForm(string s)
        {
            foreach (char c in s)
            {
                if(c == '*')
                {
                    SendKeys.SendWait("~");
                    SkipEnter = true;
                }
                else
                {
                    SendKeys.SendWait(c != ' ' ? "{" + c + "}" : " ");
                }
                await Task.Delay(50);
            }
        }

        private async Task FastFill(string s)
        {
            if (s == "*")
            {
                SendKeys.SendWait("~");
                SkipEnter = true;
            }
            else
            {
                RetryClipboard:
                try
                {
                    Clipboard.SetText(s);
                }
                catch
                {
                    //await Task.Delay(10);
                    goto RetryClipboard;
                }

                await RightClick();
            }
        }

        bool SkipEnter = false;
        OcrInput input;
        IronTesseract ocr;

        private string OCRGetScreenContent()
        {
            string TempPNG = Environment.GetEnvironmentVariable("TEMP") + "\\MATRIX" + new Random().Next(1111, 9999).ToString() + ".png";
            ScreenCapture c = new ScreenCapture();
            c.CaptureScreenToFile(TempPNG,ImageFormat.Png);
            string Input = "";
            using(input = new OcrInput(TempPNG))
            {
                input.TargetDPI = 150;
                input.MinimumDPI = 150;
                Input = ocr.Read(input).Text;
            }
            File.Delete(TempPNG);
            return Input;
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            int CurrentTop = Top, CurrentLeft = Left;
            button1.Enabled = false;
            for(int i = 2; i >= 0; i--)
            {
                Text = "Starting in " + i;
                await Task.Delay(1000);
            }
            SendKeys.SendWait("{F11}");
            Rectangle workingArea = Screen.GetWorkingArea(this);
            this.Location = new Point(workingArea.Right - Size.Width,
                workingArea.Bottom - Size.Height);
            string MatrixFile = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\MatrixType.txt";
            Stopwatch d = new Stopwatch();
            d.Start();
            button2.Visible = true;
            foreach (string s in File.ReadAllLines(MatrixFile))
            {
                string OldOCR = "";
                if(ocrCheckbox.Checked)
                {
                    OldOCR = OCRGetScreenContent();   
                }
                string NewOCR = OldOCR;

                if (s.Length <= 5)
                {
                    await FillForm(s);
                }
                else
                {
                    await FastFill(s);
                }

                if (ocrCheckbox.Checked)
                {
                    while (NewOCR == OldOCR)
                    {
                        NewOCR = OCRGetScreenContent();
                        await Task.Delay(10);
                    } 
                }

                if (!SkipEnter)
                {
                    SendKeys.SendWait("~");
                }
                if (SkipEnter) SkipEnter = false;
                int Out = 0;
                if (Int32.TryParse(Delay.Text, out Out))
                {
                    await Task.Delay(Out);
                }
                if(Stop)
                {
                    Stop = false;
                    break;
                }
                await Task.Delay(10);
            }
            d.Stop();
            Text = "Elapsed Time: " + d.Elapsed.Minutes.ToString("0") + " minutes and " + d.Elapsed.Seconds.ToString("0") + " seconds!";
            button2.Visible = false;
            SendKeys.SendWait("{F11}");
            Top = CurrentTop;
            Left = CurrentLeft;
            Visible = true;
            button1.Enabled = true;
        }

        private async Task RightClick()
        {
            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.RightDown);
            await Task.Delay(10);
            MouseOperations.MouseEvent(MouseOperations.MouseEventFlags.RightUp);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Stop = true;
        }
    }

    public class MouseOperations
    {
        [Flags]
        public enum MouseEventFlags
        {
            LeftDown = 0x00000002,
            LeftUp = 0x00000004,
            MiddleDown = 0x00000020,
            MiddleUp = 0x00000040,
            Move = 0x00000001,
            Absolute = 0x00008000,
            RightDown = 0x00000008,
            RightUp = 0x00000010
        }

        [DllImport("user32.dll", EntryPoint = "SetCursorPos")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetCursorPos(int x, int y);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetCursorPos(out MousePoint lpMousePoint);

        [DllImport("user32.dll")]
        private static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

        public static void SetCursorPosition(int x, int y)
        {
            SetCursorPos(x, y);
        }

        public static void SetCursorPosition(MousePoint point)
        {
            SetCursorPos(point.X, point.Y);
        }

        public static MousePoint GetCursorPosition()
        {
            MousePoint currentMousePoint;
            var gotPoint = GetCursorPos(out currentMousePoint);
            if (!gotPoint) { currentMousePoint = new MousePoint(0, 0); }
            return currentMousePoint;
        }

        public static void MouseEvent(MouseEventFlags value)
        {
            MousePoint position = GetCursorPosition();

            mouse_event
                ((int)value,
                 position.X,
                 position.Y,
                 0,
                 0)
                ;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MousePoint
        {
            public int X;
            public int Y;

            public MousePoint(int x, int y)
            {
                X = x;
                Y = y;
            }
        }
    }
}
