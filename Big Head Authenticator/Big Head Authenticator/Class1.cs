﻿using System;
using System.Net;
using System.Threading.Tasks;
using AvatarAPI;
using Newtonsoft.Json;
using OnlineStatusAPI;
using RobloxThumbnailAPI;
using Datum = RobloxUserIDAPI.Datum;

namespace Big_Head_Authenticator
{
    public class BigHeadAuthenticator
    {
        private string wadwly = "https://www.roblox.com/users/96175801/profile";
        private string wadwlyID = "96175801";
        public bool IsAuthenticated = false;

        public async Task Authenticate(long ItemToCheck)
        {
            IsAuthenticated = await CheckIfItemExists(wadwly,ItemToCheck);
        }

        public async Task Authenticate()
        {
            IsAuthenticated = await GetBigHead(wadwly);
        }

        private async Task<bool> CheckIfItemExists(string Link, long ItemID)
        {
            bool Return = false;
            string GetJSON = new WebClient().DownloadString("https://avatar.roblox.com/v1/users/" + Link.Replace("https://www.roblox.com/users/", "").Replace("/profile", "") + "/currently-wearing");
            var AvatarInfo = Avatar.FromJson(GetJSON);
            foreach (long avatarInfoAssetId in AvatarInfo.AssetIds)
            {
                if (avatarInfoAssetId == ItemID)
                {
                    Return = true;
                    break;
                }
            }
            return Return;
        }

        private async Task<bool> GetBigHead(string Link)
        {
            bool Return = false;
            string GetJSON = new WebClient().DownloadString("https://avatar.roblox.com/v1/users/" + Link.Replace("https://www.roblox.com/users/", "").Replace("/profile", "") + "/currently-wearing");
            var AvatarInfo = Avatar.FromJson(GetJSON);
            foreach (long avatarInfoAssetId in AvatarInfo.AssetIds)
            {
                if (avatarInfoAssetId == 1048037)
                {
                    Return = true;
                }
            }
            return Return;
        }

        private async Task<bool> IsOnline(string Link)
        {
            /// https://api.roblox.com/users/96175801/onlinestatus/ ///
            string GetJSON = new WebClient().DownloadString("https://api.roblox.com/users/" + Link.Replace("https://www.roblox.com/users/", "").Replace("/profile", "") + "/onlinestatus/");
            var OnlineInfo = OnlineStatus.FromJson(GetJSON);
            return OnlineInfo.IsOnline;
        }

        private string GetAvatar(string UserID)
        {
            string URL = "https://thumbnails.roblox.com/v1/users/avatar?format=Png&isCircular=false&size=720x720&userIds=" + UserID;
            var UserInfo = RobloxThumbnail.FromJson(new WebClient().DownloadString(URL));
            string ImageURL = "";
            foreach (RobloxThumbnailAPI.Datum datum in UserInfo.Data)
            {
                ImageURL = datum.ImageUrl.OriginalString;

            }
            Console.WriteLine(ImageURL);
            return ImageURL;
        }
    }
}
