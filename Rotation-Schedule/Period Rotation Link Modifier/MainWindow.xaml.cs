﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;

namespace Period_Rotation_Link_Modifier
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Title = "Change rotation links";
            ResizeMode = ResizeMode.CanMinimize;
            SubmitButton.Click += async (sender, args) =>
            {
                SubmitButton.IsEnabled = false;
                string Dir = Environment.GetEnvironmentVariable("APPDATA") + "\\PeriodRotations";
                File.WriteAllText(Dir + "\\" + PeriodComboBox.Text,LinkTextBox.Text);
                SubmitButton.IsEnabled = true;
                await ShowSnackBar("Link was changed to " + LinkTextBox.Text);
            };
        }

        SnackbarMessageQueue Queue = new SnackbarMessageQueue();
        private async Task ShowSnackBar(string message)
        {
            SnackHui.MessageQueue = Queue;
            Queue.Enqueue(message);
        }
    }
}
