﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Rotation_Schedule
{
    public static class IFTTT
    {
        public static async Task TriggerEvent(string Event)
        {
            HttpClient httpclient = new HttpClient();
            var values = new Dictionary<string, string>
            {

            };
            var content = new FormUrlEncodedContent(values);
            string EventValue = Event;
            BackgroundWorker Poster = new BackgroundWorker();
            Poster.DoWork += async (sender, args) =>
            {
                await httpclient.PostAsync("https://maker.ifttt.com/trigger/" + EventValue + "/with/key/c9uxOoCGKFqAVfoNm_xOh7", content);
            };
            Poster.RunWorkerAsync();
        }

        public static async Task TriggerEvent(string Event, string Value1, string Value2, string Value3)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://maker.ifttt.com/trigger/" + Event + "/with/key/c9uxOoCGKFqAVfoNm_xOh7");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{ \"value1\" : \"" + Value1 + "\", \"value2\" : \"" + Value2 + "\", \"value3\" : \"" + Value3 + "\" }";

                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
            }
        }
        public static async Task LightsOn()
        {
            await TriggerEvent("turn_on_cn_light");
        }

        public static async Task LightsOff()
        {
            await TriggerEvent("turn_off_cn_lights");
        }

        public static async Task ChangeLightColour(string color)
        {
            await TriggerEvent("cn_light_" + color);
        }

        public static async Task TeacherAttention(string AttentionWord)
        {
            await TriggerEvent("teacher_attention",AttentionWord,"","");
        }
    }
}
