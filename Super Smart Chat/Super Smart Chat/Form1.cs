﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using IronOcr;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Super_Smart_Chat
{
    public partial class Form1 : Form
    {
        private Timer timer;
        private string previousText1 = "";
        int counter = 0;
        private string previousText2 = "";
        string ResponseRAM = "";

        public string SendRequest(string prompt)
        {
            // Create the request data as a JSON string
            string json = "{\"model\": \"text-davinci-003\", \"prompt\": \"" + prompt + "\", \"max_tokens\": " + ResponseLength.Text + ", \"temperature\": " + TempuratureTextBox.Text + "}";

            // Create the request object
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://api.openai.com/v1/completions");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers.Add("Authorization", "Bearer " + "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");

            // Write the request data to the request stream
            using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
            {
                writer.Write(json);
            }
            string text = "";
            // Send the request and read the response
            try
            {
                string responseText;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    responseText = reader.ReadToEnd();
                }
                //MessageBox.Show(responseText);
                // Parse the response text as JSON
                JObject jsonResponse = JObject.Parse(responseText);

                // Get the text from the first element of the "choices" array
                text = (string)jsonResponse["choices"][0]["text"];
            }
            catch 
            {
                text = "No response";
            }

            return text;
        }

        public Form1()
        {
            IronOcr.Installation.LicenseKey = "IRONOCR.WILLROBO16.5467-DFFDC0E795-BHZ27FM2YTL4B-YUO2ZLG4WFDL-7KA3UPPSXUBB-X7P7BAUMSOBZ-5QPM5GOFEXGH-XQPO5N-TZ3XH2XJDJCIUA-DEPLOYMENT.TRIAL-MZTG6H.TRIAL.EXPIRES.11.JAN.2023";
            InitializeComponent();

            // Create a timer that fires every 1000 milliseconds (1 second)
            timer = new Timer();
            timer.Interval = 1000;
            timer.Tick += Timer_Tick;
            timer.Start();
            ResponseBox.DetectUrls = true;
            // Select the text in the textbox
            QuestionBox1.Select();
            Load += Form1_Load;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //QuestionBox1.Text = new IronTesseract().Read(@"C:\Users\cntow\Downloads\testocrimage.png").Text;
        }

        private void QuestionBox2_TextChanged(object sender, EventArgs e)
        {
            QuestionBox1.Text = QuestionBox2.Text;
        }

        private void QuestionBox1_TextChanged(object sender, EventArgs e)
        {
            QuestionBox2.Text = QuestionBox1.Text;
        }

        private async void Timer_Tick(object sender, EventArgs e)
        {
            // Get the current text in the text boxes
            string currentText1 = QuestionBox1.Text;
            string currentText2 = QuestionBox2.Text;

            // Check if the text in the text boxes hasn't changed since the last time we checked
            if (currentText1 == previousText1 && currentText2 == previousText2 && previousText1 != MasterText && previousText2 != MasterText && QuestionBox1.Text != "")
            {
                // If the text hasn't changed, increment the counter
                counter++;
                CounterLabel.Text = "Update in: " + (2 - counter);
                // If the counter reaches 3, it means that there's been no change in the text
                // for 3 seconds, so we can run the function
                if (counter == 2 && AutoRunCheckBox.Checked)
                {
                    counter = 0;
                    MasterText = previousText1;
                    ResponseBox.BackColor = Color.LightGreen;
                    await RunFunction();
                }
                else if(AutoRunCheckBox.Checked == false)
                {
                    counter = 0;
                }
            }
            else if(QuestionBox2.Text == "")
            {
                CounterLabel.Text = "Write a question";
            }
            else
            {
                if(previousText1 != MasterText)
                    ResponseBox.BackColor = Color.Pink;
                else ResponseBox.BackColor = Color.White;
                // If the text has changed, reset the counter
                counter = 0;
                CounterLabel.Text = "Ready";
            }

            // Save the current text as the previous text for the next time we check
            previousText1 = currentText1;
            previousText2 = currentText2;
        }

        string MasterText = "";

        private async Task RunFunction()
        {
            Text = "Thinking...";
            QuestionBox2.Enabled = false;
            StartButton.Enabled = false;
            QuestionBox1.Enabled = false;
            // This is where you would put the code for the function you want to run
            string LOL = "";
            string t = QuestionBox2.Text;
            await Task.Factory.StartNew(() =>
            {
                LOL = SendRequest(/*ResponseRAM + "\n" +*/ t); 
                Console.WriteLine(LOL);
            });
            ResponseBox.Text = LOL;
            Text = "Done";
            QuestionBox2.Enabled = true;
            StartButton.Enabled = true;
            QuestionBox1.Enabled = true;
            ResponseRAM += " " + ResponseBox.Text;

            Regex regex = new Regex(@"\b(?:https?://|www\.)\S+\b", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            if (AutoURL.Checked)
            {
                if (regex.IsMatch(ResponseBox.Text))
                {
                    string url = regex.Match(ResponseBox.Text).Value;
                    Process.Start(url);
                } 
            }

            QuestionBox2.Select();
        }

        private async void StartButton_Click(object sender, EventArgs e)
        {
            if (QuestionBox2.Text != "")
            {
                StartButton.Enabled = false;
                bool StoreAutoRunCheckBox = AutoRunCheckBox.Checked;
                if (StoreAutoRunCheckBox)
                {
                    AutoRunCheckBox.Enabled = false;
                    AutoRunCheckBox.Checked = false;
                }
                await RunFunction();
                if (StoreAutoRunCheckBox)
                {
                    AutoRunCheckBox.Enabled = true;
                    AutoRunCheckBox.Checked = true;
                }
                StartButton.Enabled = true; 
            }
        }

        private void EnableRichText_Click(object sender, EventArgs e)
        {
            ResponseBox.ReadOnly = false;
        }

        private void DisableRichText_Click(object sender, EventArgs e)
        {
            ResponseBox.ReadOnly = true;
        }

        private void CopyTextButton_Click(object sender, EventArgs e)
        {
            try
            {
                Clipboard.SetText(ResponseBox.Text);
            }
            catch 
            {
                
            }
        }

        bool Lock1 = false;
        bool Lock2 = false;

        private void QuestionBox1_TextChanged_1(object sender, EventArgs e)
        {
            if (!Lock2)
            {
                Lock1 = true;
                QuestionBox2.Text = QuestionBox1.Text;
                Lock1 = false;
            }
        }

        private void QuestionBox2_TextChanged_1(object sender, EventArgs e)
        {
            if (!Lock1)
            {
                Lock2 = true;
                QuestionBox1.Text = QuestionBox2.Text;
                Lock2 = false;
            }
        }

        public string GetTextFromImage(string fileName)
        {
            return new IronTesseract().Read(fileName).Text;
        }


        private void UploadImageButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog d = new OpenFileDialog();
            try
            {
                d.ShowDialog();
                if(File.Exists(d.FileName))
                {
                    string ImageFile = "";
                    if (d.FileName.EndsWith(".png") || d.FileName.EndsWith(".jpeg") || d.FileName.EndsWith(".jpg"))
                    {
                        // Do something
                        ImageFile = d.FileName;
                    }
                    else
                    {
                        
                        // Convert the file to a PNG file
                        Image image = Image.FromFile(d.FileName);
                        image.Save(d.FileName + ".png", ImageFormat.Png);
                        ImageFile = d.FileName + ".png";
                    }
                    Console.WriteLine(ImageFile);
                    QuestionBox1.Text = GetTextFromImage(ImageFile);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }
    }
}
