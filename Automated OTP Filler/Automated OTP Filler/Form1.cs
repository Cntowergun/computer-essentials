﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Speech.Synthesis;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using MimeKit;
using UsefulTools;

namespace Automated_OTP_Filler
{
    public partial class Form1 : Form
    {
        SpeechSynthesizer speech = new SpeechSynthesizer();

        public Form1()
        {
            InitializeComponent();
            MaximizeBox = false;
            FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        public static string PostJson(string Json, string Url)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = Json;

                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            string result = "";
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            return result;
        }

        Email email;
        private static string Email_ = Environment.GetEnvironmentVariable("APPDATA") + "\\OTP" + "\\User.txt";
        private static string Password_ = Environment.GetEnvironmentVariable("APPDATA") + "\\OTP" + "\\Password.txt";
        private static string CredentialsDirectory = Environment.GetEnvironmentVariable("APPDATA") + "\\OTP";
        private async void Form1_Load(object sender, EventArgs e)
        {
            if (!Directory.Exists(CredentialsDirectory))
            {
                Visible = true;
                while (!Directory.Exists(CredentialsDirectory))
                {
                    await Task.Delay(10);
                }
            }
            email = new Email(File.ReadAllText(Email_),File.ReadAllText(Password_));
            while (true)
            {
                Lol:
                try
                {
                    await Task.Delay(100);
                    Visible = false;
                    var Emails = email.GetEmails();
                    foreach (MimeMessage s in Emails)
                    {
                        // Start Checking Here //
                        Console.WriteLine(s.Subject);
                        if(CheckNoCase(s.Subject,"my phone number!"))
                        {
                            speech.Speak("We received a number");
                            string Lol = s.TextBody;
                            foreach (string s1 in Lol.Split(new []{Environment.NewLine},StringSplitOptions.None))
                            {
                                if(s1.Contains("Hey there! Letting you know this is my number where you can reach me anytime:"))
                                {
                                    Lol = s1.Split(':')[1].Trim();
                                    Lol = Lol.Replace(" ", "");
                                    Lol = Lol.Replace("(", "").Replace(")", "").Replace("-", "");
                                    Lol = Lol.Replace(" ", "");
                                    File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\GrabbedPhoneNumber.txt",Lol);
                                    email.DeleteMessage("my phone number!");
                                    PostJson(Lol, "http://dashing-tools.bigheados.com/set_phone_number");
                                    speech.Speak("Number finished");
                                    break;
                                }
                            }
                        }
                        if (CheckNoCase(s.Subject, "Activate Apple Pay"))
                        {
                            string UnHTML = HtmlToText.ConvertHtml(s.HtmlBody);
                            File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\ApplePayCodeEmail.txt", UnHTML);
                            string[] ha = File.ReadAllLines(Environment.GetEnvironmentVariable("TEMP") + "\\ApplePayCodeEmail.txt");
                            foreach (string s1 in ha)
                            {
                                if(s1.Contains("Pour activer Apple Pay"))
                                {
                                    foreach (string s2 in s1.Split(','))
                                    {
                                        if(s2.Contains("appuyez sur votre carte et entrez le code"))
                                        {
                                            string Code = s2.Replace("appuyez sur votre carte et entrez le code", "").Replace(" ", "");
                                            Code = Code.Split('.')[0];
                                            File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\ApplePayPureCode.txt",Code);
                                        }
                                    }
                                }
                            }
                            email.DeleteMessage(s.Subject);
                        }
                        if (CheckNoCase(s.Subject,"Verify your KOHO"))
                        {
                            //string UnHTML = HtmlToText.ConvertHtml(s.HtmlBody);
                            File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\ApplePayVerifiedEmail.txt",s.TextBody);
                            email.DeleteMessage(s.Subject);
                        }

                        if(CheckNoCase(s.Subject, "We detected a suspicious login attempt"))
                        {
                            speech.Speak("Approving the wise login request");
                            // Wise Handler //
                            string UnHTML = HtmlToText.ConvertHtml(s.HtmlBody);
                            string WiseURL = ExtractWiseUrl(UnHTML);

                            // Replace with the URL you want to navigate to
                            string url = WiseURL;
                            //MessageBox.Show(url);
                            url = url.Replace("<", "").Replace(">", "");
                            // Initialize the process and set up the start info
                            Process process = new Process();
                            Clipboard.SetText(url);
                            process.StartInfo.FileName = @"C:\Program Files\Google\Chrome\Application\chrome.exe";  // Path to Chrome executable
                            process.StartInfo.Arguments = "--headless --disable-gpu --remote-debugging-port=9222 \"" + url + "\"";
                            // Run the process
                            process.Start();

                            email.DeleteMessage(s.Subject);
                        }

                        if (CheckNoCase(s.Subject, "Gmail Forwarding Confirmation - Receive Emails from"))
                        {
                            foreach (string s1 in s.TextBody.Split(new[] { Environment.NewLine }, StringSplitOptions.None))
                            {
                                if (CheckNoCase(s1, "Confirmation code"))
                                {
                                    Clipboard.SetText(s1.Split(':')[1].Trim().Replace(" ", ""));
                                    goto Lol;
                                }
                            }
                            email.DeleteMessage(s.Subject);
                        }
                        else if (CheckNoCase(s.Subject, "Roblox Login Request"))
                        {
                            string UnHTML = HtmlToText.ConvertHtml(s.HtmlBody);
                            foreach (string s1 in UnHTML.Split(new []{Environment.NewLine},StringSplitOptions.None))
                            {
                                if (s1.Contains("is your Roblox 2-Step Verification code"))
                                {
                                    UnHTML = s1.Split('i')[0].Trim().Replace(" ", "");
                                }
                            }
                            Clipboard.SetText(UnHTML);
                            await FillForm(UnHTML);
                            SendKeys.SendWait("~");
                            email.DeleteMessage(s.Subject);
                        }
                        else if (CheckNoCase(s.Subject, "PayPal: Your security code is"))
                        {
                            foreach (string s1 in s.TextBody.Split(new []{Environment.NewLine},StringSplitOptions.None))
                            {
                                if (s1.Contains("Your security code"))
                                {
                                    string Code = s1.Split(':')[2].Split('.')[0].Trim().Replace(" ", "");
                                    Clipboard.SetText(Code);
                                    await FillForm(Code);
                                    SendKeys.SendWait("~");
                                    email.DeleteMessage(s.Subject);
                                    goto Lol;
                                }
                            }
                        }
                        else if (CheckNoCase(s.Subject, "is your Amazon OTP. Do not share it with anyone."))
                        {
                            string Code = s.Subject.Split('i')[0].Trim().Replace(" ", "");
                            Clipboard.SetText(Code);
                            await FillForm(Code);
                            SendKeys.SendWait("~");
                            email.DeleteMessage(s.Subject);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }

        private bool CheckNoCase(string Source, string Compare)
        {
            return new CultureInfo("en-US").CompareInfo.IndexOf(Source, Compare, CompareOptions.IgnoreCase) >= 0;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (Username.Text != "" && Password.Text != "")
            {
                Directory.CreateDirectory(CredentialsDirectory);
                File.WriteAllText(CredentialsDirectory + "\\User.txt",Username.Text);
                File.WriteAllText(CredentialsDirectory + "\\Password.txt", Password.Text);
            }
        }
        public static string ExtractWiseUrl(string inputString)
        {
            // Updated pattern to match the URL, note the "approveLink" and "&amp;"
            string pattern = @"https:\/\/api\.transferwise\.com\/v1\/notification-flow\/messages\/[a-zA-Z0-9\-]+\/deliveries\/[a-zA-Z0-9\-]+\/linkClicks\?.*?name=approveLink.*(&amp;).*";

            Match match = Regex.Match(inputString, pattern);

            if (match.Success)
            {
                return match.Value.Replace("&amp;","&");
            }
            else
            {
                return "No matching URL found";
            }
        }

        private async Task FillForm(string Stuff)
        {
            foreach (char c in Stuff)
            {
                SendKeys.SendWait("{" + c + "}");
                await Task.Delay(100);
            }
        }
    }
}
