﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NextbusNET;
using NextbusNET.Model;

namespace Bus_Schedule
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ResizeMode = ResizeMode.CanMinimize;
            Title = "Real-Time Display";
        }

        NextbusNET.NextbusClient bus = new NextbusClient();
        
        private async void BroadcastButton_Click(object sender, RoutedEventArgs e)
        {
            await BroadcastBus("300", "5189");
        }

        private async Task BroadcastBus(string route, string stop)
        {
            IEnumerable<Prediction> Predictions = new List<Prediction>();
            await Task.Factory.StartNew(() =>
            {
                Predictions = bus.GetPredictions("ttc", stop, route);
            });
            foreach (Prediction prediction in Predictions)
            {
                Console.WriteLine(prediction.Minutes);
                TopDisplayLabel.Content = "68 - Warden Station " + prediction.Minutes + " min";
            }
        }
    }
}
